package main

import (
	"websoket/controller"
	"websoket/webscoket/Init"
)

func main() {
	controller.Run() //信息处理
	Init.Heartbeat_time = 200
	Init.Heartbeat_interval = 100
	Init.Port = 8000
	Init.Run() //初始化websocket
}
