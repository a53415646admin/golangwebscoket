package controller

import (
	"encoding/json"
	"fmt"
	"log"
	"websoket/webscoket/Init"
	"websoket/webscoket/Template"
)

//心跳检测
type Heartbeat struct {
	Type   int `json:"type"`
	Device int `json:"device"`
}

func Run() {
	getmsg()
}

func getmsg() {
	go func() {
		for {
			select {
			case msg := <-Init.Msgchan: //发信息
				action(msg)
			}
		}
	}()
}

func action(s Template.Typemsg) {
	var rejson *Heartbeat
	err := json.Unmarshal([]byte(s.Msg), &rejson)
	if err != nil {
		log.Println(s.Key, "发送的json是错误的", s.Msg)
		return
	}
	types := rejson.Type
	if types == 1 {
		fmt.Println("心跳检测")
		ws, _ := Init.Getmap(s.Key)
		ws.Listmap.WriteMessage(1, []byte("服务器收到,你的信息"))
	}

	if types == 2 {
		ws, err := Init.Getmap(s.Key)
		if err != nil {
			log.Printf("连接已被关闭")
			return
		}
		err = ws.Listmap.WriteMessage(1, []byte("服务器收到,你的信息"))
		log.Println("发送数据成功")
		if err != nil {
			fmt.Println("长连接信息发送失败--", err)
		}

	}
}
