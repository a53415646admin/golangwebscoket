package main

import (
	"fmt"
	"websoket/client/clientwebscoket"
)

var num, sendnum int
var wspath, msg string

func main() {
	fmt.Println("WS高并发压力测试 v1.0 QQ478120174 \n系统会根据发起请求次数创建大量的协程进行连接在创建好所有连接后，在进行高并发的发送信息")
	fmt.Print("输入WS连接数量：")
	fmt.Scanln(&num)
	fmt.Print("输入单个连接发送信息数量：")
	fmt.Scanln(&sendnum)
	fmt.Print("输入WS接地址：")
	fmt.Scanln(&wspath)
	fmt.Print("输入要发送的字符串：")
	fmt.Scanln(&msg)
	clientwebscoket.Num = num
	clientwebscoket.Sendnum = sendnum
	clientwebscoket.Wspath = wspath
	clientwebscoket.Sendmsg = msg
	go func() {
		for {
			fmt.Println(<-clientwebscoket.Msg)
		}
	}()
	clientwebscoket.Run()
}
