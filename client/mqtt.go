package main

import (
	"fmt"
	"github.com/eclipse/paho.mqtt.golang"
	"log"
	"time"
)

var f mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
	fmt.Printf("TOPIC: %s\n", msg.Topic())
	fmt.Printf("MSG: %s\n", msg.Payload())
}

func main() {
	opts := mqtt.NewClientOptions().AddBroker("tcp://192.168.1.127:1883").SetClientID("mqtt_client11")

	//set userName
	opts.SetUsername("longrbl")
	opts.Password = "123456"
	opts.SetKeepAlive(61 * time.Second)
	opts.SetDefaultPublishHandler(f)
	opts.SetPingTimeout(1 * time.Second)

	//create object
	c := mqtt.NewClient(opts)

	if token := c.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}

	token := c.Subscribe("task/longrbl", 1, nil)

	if token.Wait() {
		fmt.Println(token.Error())
	}
		//
	//for i := 0; i < 5000000; i++ {
	//	text := fmt.Sprintf("{ \"name\": \"longrbl\" }", i)
	//	token := c.Publish("task/longrbl", 0, false, text)
	//	token.Wait()
	//	time.Sleep(time.Millisecond * 1)
	//}

	log.Println("ok")
	time.Sleep(time.Second * 52)
}
