package clientwebscoket

import (
	"fmt"
	"github.com/gorilla/websocket"
	"sync"
	"time"
)

var sum = make(chan int, 100) //创建
var wss = make(chan *websocket.Conn, 50)
var wg = &sync.WaitGroup{}
var Wspath string = "127.0.0.1:8000/ws?token="
var conmap = &sync.Map{}
var state = make(chan int, 50)
var wgd = &sync.WaitGroup{}
var Num int = 10000 //创建长连接的数量
var Sendnum = 1     //每个长连接发送信息的次数
var openchan int = Num / 100
var senchan int = Num / 10
var Msg = make(chan string)

var Sendmsg string = "1" //要发送的数据

func Run() {
	cg := 0
	sbai := 0
	sendcg := 0
	sendsbai := 0

	go func() {
		for ko := range state {
			switch ko {
			case 1:
				cg++
			case 2:
				sbai++
			case 3:
				sendcg++
			case 4:
				sendsbai++
			}
		}
	}()
	for i := 0; i < openchan; i++ {
		wg.Add(1)
		go openws()
	}
	Msg <- "开始创建连接。"
	for i := 0; i < Num; i++ {
		sum <- i
	}
	close(sum)
	wg.Wait()
	Msg <- "创建连接成功。"
	Msg <- "开始发送数据。"

	for i := 0; i < senchan; i++ {
		wgd.Add(1)
		go send()
	}
	conmap.Range(func(key, value interface{}) bool {
		wss <- value.(*websocket.Conn)
		return true
	})
	close(wss)
	wgd.Wait()
	Msg <- "发送数据结束。"
	Msg <- fmt.Sprint("成功连接：", cg, "次 连接失败：", sbai, "次 发送消息成功：", sendcg, "次 发送消息失败：", sendsbai, "次")
	time.Sleep(time.Second * 10)
}

func openws() {
	defer wg.Done()
	for i := range sum {
		ws, _, err := websocket.DefaultDialer.Dial(fmt.Sprint("ws://", Wspath), nil)
		if err != nil {
			state <- 2
			continue
		}
		conmap.Store(i, ws)
		state <- 1
		time.Sleep(time.Nanosecond * 1)
	}
}

func send() {
	defer wgd.Done()
	for wss := range wss {
		if wss == nil {
			break
		}
		for i := 0; i < Sendnum; i++ {
			err := wss.WriteMessage(1, []byte(Sendmsg))
			if err != nil {
				state <- 4
				continue
			}
			state <- 3
		}
		time.Sleep(time.Nanosecond * 1)
	}

}
