package Init

import (
	"errors"
	"fmt"
	"github.com/gorilla/websocket"
	"log"
	"net/http"
	"sync"
	"time"
	"websoket/webscoket/Template"
)

// webscoket  升级协议
var Cli int64 = 1
var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

type Connmap struct {
	Listmap *websocket.Conn //ws对象
	Stime   int64           //最后发生信息时间
	Key     string          //ws唯一标识
	Fd      int64           //唯一标识
}

var Msgchan = make(chan Template.Typemsg, 300)

var Clientset = sync.Map{} //所有长连接集合

var Heartbeat_time int64 = 20     //心跳(秒)
var Heartbeat_interval int64 = 60 //心跳检测间隔(秒)
var Port = 8000

func Run() {
	go heartbeat()
	// Configure websocket route
	http.HandleFunc("/", getwebscoketstate)
	http.HandleFunc("/ws", handleConnections)

	// Start the server on localhost port 8000 and log any errors
	log.Println("http server started on :8000")
	err := http.ListenAndServe(fmt.Sprint(":", Port), nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}

}

func getwebscoketstate(w http.ResponseWriter, r *http.Request) {

	Clii := 0
	Clientset.Range(func(key, value interface{}) bool {
		Clii++
		return true
	})
	w.Write([]byte(fmt.Sprint("当前长连接数量", Clii)))
}

//心跳检测
func heartbeat() {
	for {
		Clientset.Range(func(key, value interface{}) bool {
			times := time.Now().Unix()
			ws := value.(Connmap)
			wstime := ws.Stime
			if (wstime + Heartbeat_time) < times {
				ws.Listmap.Close()
			}
			time.Sleep(time.Nanosecond)
			return true
		})

		time.Sleep(time.Second * time.Duration(Heartbeat_interval))
	}
}

func handleConnections(w http.ResponseWriter, r *http.Request) {
	uid := r.FormValue("token")
	ws, err := upgrader.Upgrade(w, r, nil)
	defer ws.Close()
	if err != nil {
		log.Fatal(err)
	}
	if uid == "" {
		log.Print("没有获取到token禁止继续登录")
		ws.Close()
	}

	var msgtype Template.Typemsg
	var listmap Connmap

	listmap.Listmap = ws

	listmap.Key = uid
	listmap.Stime = time.Now().Unix()
	Clientset.Store(uid, listmap)
	for {
		msgtypw, msg, _ := ws.ReadMessage()
		if msgtypw == -1 {
			getws, ok := Clientset.Load(uid)
			if !ok {
				log.Printf("没有获取指定的数据")
				return
			}
			jiu := getws.(Connmap)
			jiu.Listmap.Close()
			Clientset.Delete(uid)
			log.Print(uid, "断开连接")
			return
		}
		msgtype.Msgtype = msgtypw
		msgtype.Msg = string(msg)
		msgtype.Key = uid
		listmap.Stime = time.Now().Unix() //更新ws最后发送信息的时间
		Clientset.Store(uid, listmap)
		Msgchan <- msgtype
		time.Sleep(time.Nanosecond)
	}
}

func Getmap(key string) (Connmap, error) {
	listmap, ok := Clientset.Load(key)
	if !ok {
		return Connmap{}, errors.New("获取的map是为空")

	}
	return listmap.(Connmap), nil
}
